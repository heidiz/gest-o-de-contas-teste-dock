package com.testedock.gc.ui;

import com.testedock.gc.domain.*;
import com.testedock.gc.domain.service.ContaService;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.PessoaRepository;
import com.testedock.gc.infrastructure.TransacaoRepository;
import com.testedock.gc.ui.controller.ContaController;
import com.testedock.gc.ui.controller.model.*;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

/**
 * Este teste deve ser executado em ordem!
 *
 */
@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class ContaControllerTest {
    @Autowired
    private ContaService contaService;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private ContaRepository contaRepository;

    private static Long numeroConta;

    @BeforeAll
    public void init() {
        transacaoRepository.deleteAll();
        contaRepository.deleteAll();
        pessoaRepository.deleteAll();
        pessoaRepository.save(new Pessoa("Lucas Souza Pessoa", "44090988888",
                LocalDate.of(1995, 6, 29)));
        pessoaRepository.save(new Pessoa("Joana aona ana", "12345678912", LocalDate.now()));
    }

    @Test
    @Order(1)
    public void criaConta() {
        ContaController contaController = new ContaController(contaService);
        NovaConta novaConta = new NovaConta();
        novaConta.setTipoConta(TipoConta.CONTACORRENTE);
        ResponseEntity<ContaResponse> contaResponse = contaController.criaConta("44090988888", novaConta);
        ContaResponse conta = contaResponse.getBody();
        assertNotNull(conta.getNumeroConta());
        numeroConta = conta.getNumeroConta();
        assertEquals(HttpStatus.CREATED, contaResponse.getStatusCode());
        PessoaResponse pessoaResponse = conta.getPessoa();
        assertEquals("Lucas Souza Pessoa", pessoaResponse.getNome());
        assertEquals("44090988888", pessoaResponse.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), pessoaResponse.getDataNascimento());
        assertEquals(Conta.LIMITE_SAQUE_DIARIO_BASE, conta.getLimiteSaqueDiario());
        assertEquals(0.0, conta.getSaldo());
    }

    @Test
    @Order(2)
    public void deposita() {
        Conta conta = contaService.findConta(numeroConta);
        ContaController contaController = new ContaController(contaService);
        Deposito deposito = new Deposito();
        deposito.setNumeroConta(conta.getId());
        deposito.setValor(1000.0);
        ResponseEntity<ContaResponse> contaResponse = contaController.deposita("12345678912", deposito);
        ContaResponse conta2 = contaResponse.getBody();
        assertEquals(HttpStatus.OK, contaResponse.getStatusCode());
        PessoaResponse pessoaResponse = conta2.getPessoa();
        assertEquals("Lucas Souza Pessoa", pessoaResponse.getNome());
        assertEquals("44090988888", pessoaResponse.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), pessoaResponse.getDataNascimento());
        assertEquals(1000.0, conta2.getSaldo());
    }

    @Test
    @Order(3)
    public void contaSaldo() {
        ContaController contaController = new ContaController(contaService);
        ResponseEntity<SaldoResponse> contaResponse = contaController.consultaSaldo(numeroConta);
        SaldoResponse saldoConta = contaResponse.getBody();
        assertEquals(HttpStatus.OK, contaResponse.getStatusCode());
        assertEquals(numeroConta, saldoConta.getNumeroConta());
        assertEquals(1000.0, saldoConta.getSaldo());

        PessoaResponse pessoaResponse = saldoConta.getPessoa();
        assertEquals("Lucas Souza Pessoa", pessoaResponse.getNome());
        assertEquals("44090988888", pessoaResponse.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), pessoaResponse.getDataNascimento());
    }

    @Test
    @Order(4)
    public void saca() {
        ContaController contaController = new ContaController(contaService);
        Saque saque = new Saque();
        saque.setValor(300.0);
        ResponseEntity<ContaResponse> contaResponse = contaController.saca(numeroConta, saque);
        ContaResponse conta2 = contaResponse.getBody();
        assertEquals(HttpStatus.OK, contaResponse.getStatusCode());
        PessoaResponse pessoaResponse = conta2.getPessoa();
        assertEquals("Lucas Souza Pessoa", pessoaResponse.getNome());
        assertEquals("44090988888", pessoaResponse.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), pessoaResponse.getDataNascimento());
        assertEquals(700.0, conta2.getSaldo());
    }

    @Test
    @Order(5)
    public void pegaExtrato() {
        ContaController contaController = new ContaController(contaService);
        Conta conta = contaService.findConta(numeroConta);
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO,
                LocalDateTime.of(LocalDate.of(2020, 2, 10), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 400.0, TipoTransacao.SAQUE,
                LocalDateTime.of(LocalDate.of(2020, 2, 11), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 100.0, TipoTransacao.SAQUE,
                LocalDateTime.of(LocalDate.of(2020, 2, 12), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        Periodo periodo = new Periodo();
        periodo.setInicio(LocalDate.of(2020, 2, 10));
        periodo.setFim(LocalDate.of(2020, 2, 12));

        ResponseEntity<Extrato> extratoResponse = contaController.pegaExtrato(numeroConta, periodo);
        Extrato extrato = extratoResponse.getBody();
        ContaResponse contaResponse = extrato.getConta();
        assertEquals(HttpStatus.OK, extratoResponse.getStatusCode());
        PessoaResponse pessoaResponse = contaResponse.getPessoa();
        assertEquals("Lucas Souza Pessoa", pessoaResponse.getNome());
        assertEquals("44090988888", pessoaResponse.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), pessoaResponse.getDataNascimento());
        assertEquals(1200.0, contaResponse.getSaldo());
        List<TransacaoResponse> transacoes = extrato.getTransacoes();
        assertEquals(3, transacoes.size());

        TransacaoResponse transacaoResponse = transacoes.get(0);
        assertEquals("Depósito", transacaoResponse.getTipoTransacao());
        assertEquals(1000.0, transacaoResponse.getValor());
        assertEquals(transacaoResponse.getDataTransacao().toLocalDate(),
                LocalDate.of(2020, 2, 10));

        transacaoResponse = transacoes.get(1);
        assertEquals("Saque", transacaoResponse.getTipoTransacao());
        assertEquals(400.0, transacaoResponse.getValor());
        assertEquals(transacaoResponse.getDataTransacao().toLocalDate(), LocalDate.of(2020, 2, 11));

        transacaoResponse = transacoes.get(2);
        assertEquals("Saque", transacaoResponse.getTipoTransacao());
        assertEquals(100.0, transacaoResponse.getValor());
        assertEquals(transacaoResponse.getDataTransacao().toLocalDate(), LocalDate.of(2020, 2, 12));
    }
}
