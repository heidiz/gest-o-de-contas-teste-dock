package com.testedock.gc.domain;

import com.testedock.gc.domain.service.ContaService;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.PessoaRepository;
import com.testedock.gc.infrastructure.TransacaoRepository;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;
import com.testedock.gc.infrastructure.exceptions.ForbiddenException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class ContaTest {
    @Autowired
    private ContaService contaService;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private ContaRepository contaRepository;

    @BeforeEach
    public void resetDados() {
        transacaoRepository.deleteAll();
        contaRepository.deleteAll();
        pessoaRepository.deleteAll();
        pessoaRepository.save(new Pessoa("Lucas Souza Pessoa", "44090988888",
                LocalDate.of(1995, 6, 29)));
    }

    @Test
    public void transacionaDeposito() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        assertEquals(0.0, conta.getSaldo());
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        assertEquals(TipoTransacao.DEPOSITO, transacao.getTipoTransacao());
        assertEquals(1000.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.now());
        assertEquals(1000.0, conta.getSaldo());
    }

    @Test
    public void transacionaSaque() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        assertEquals(0.0, conta.getSaldo());
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        transacao = new Transacao(null, conta, 400.0, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        assertEquals(TipoTransacao.SAQUE, transacao.getTipoTransacao());
        assertEquals(400.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.now());
        assertEquals(600.0, conta.getSaldo());
    }

    @Test
    public void saldoVariavel() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        assertEquals(0.0, conta.getSaldo());
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        transacao = new Transacao(null, conta, 400.0, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        transacao = new Transacao(null, conta, 100.0, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        transacao = new Transacao(null, conta, 350.0, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        transacao = new Transacao(null, conta, 2500.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        assertEquals(2650.0, conta.getSaldo());
    }

    @Test
    public void bloqueiaConta() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta.bloqueiaConta();
        conta = contaService.findConta(conta.getId());
        assertFalse(conta.isFlagAtivo());
    }

    @Test
    public void contaJaBloqueada() {
        assertThrows(BadRequestException.class, () -> {
            Pessoa lucas = contaService.findPessoa("44090988888");
            Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
            conta.bloqueiaConta();
            conta = contaService.findConta(conta.getId());
            conta.bloqueiaConta();
        });
    }

    @Test
    public void emiteExtrato() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        assertEquals(0.0, conta.getSaldo());
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO,
                LocalDateTime.of(LocalDate.of(2020, 2, 10), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 400.0, TipoTransacao.SAQUE,
                LocalDateTime.of(LocalDate.of(2020, 2, 11), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 100.0, TipoTransacao.SAQUE,
                LocalDateTime.of(LocalDate.of(2020, 2, 12), LocalTime.now()));
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 350.0, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        transacao = new Transacao(null, conta, 2500.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        assertEquals(2650.0, conta.getSaldo());
        List<Transacao> transacoes = conta.extrato(LocalDate.of(2020, 2, 12), LocalDate.now());
        assertEquals(3, transacoes.size());

        transacao = transacoes.get(0);
        assertEquals(TipoTransacao.SAQUE, transacao.getTipoTransacao());
        assertEquals(100.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.of(2020, 2, 12));

        transacao = transacoes.get(1);
        assertEquals(TipoTransacao.SAQUE, transacao.getTipoTransacao());
        assertEquals(350.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.now());

        transacao = transacoes.get(2);
        assertEquals(TipoTransacao.DEPOSITO, transacao.getTipoTransacao());
        assertEquals(2500.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.now());

        transacoes = conta.extrato(LocalDate.of(2020, 2, 10),
                LocalDate.of(2020, 2, 11));
        assertEquals(2, transacoes.size());

        transacao = transacoes.get(0);
        assertEquals(TipoTransacao.DEPOSITO, transacao.getTipoTransacao());
        assertEquals(1000.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.of(2020, 2, 10));

        transacao = transacoes.get(1);
        assertEquals(TipoTransacao.SAQUE, transacao.getTipoTransacao());
        assertEquals(400.0, transacao.getValor());
        assertEquals(transacao.getDataTransacao().toLocalDate(), LocalDate.of(2020, 2, 11));
    }

    @Test
    public void emiteExtratoContaBloqueada() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        Transacao transacao = new Transacao(null, conta, 1000.0, TipoTransacao.DEPOSITO);
        conta.setSaldo(transacao);
        conta.bloqueiaConta();
        transacaoRepository.save(transacao);
        contaRepository.save(conta);
        assertThrows(ForbiddenException.class, () -> conta.extrato(LocalDate.now(), LocalDate.now()));
    }
}
