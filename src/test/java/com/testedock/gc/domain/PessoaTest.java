package com.testedock.gc.domain;

import com.testedock.gc.domain.service.ContaService;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.PessoaRepository;
import com.testedock.gc.infrastructure.TransacaoRepository;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;
import com.testedock.gc.infrastructure.exceptions.ForbiddenException;
import com.testedock.gc.infrastructure.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
public class PessoaTest {
    @Autowired
    private ContaService contaService;

    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private ContaRepository contaRepository;

    @BeforeEach
    public void resetDados() {
        transacaoRepository.deleteAll();
        contaRepository.deleteAll();
        pessoaRepository.deleteAll();
        pessoaRepository.save(new Pessoa("Lucas Souza Pessoa", "44090988888",
                LocalDate.of(1995, 6, 29)));
    }

    @Test
    public void validaPessoa() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        assertNotNull(lucas.getId());
        assertEquals("Lucas Souza Pessoa", lucas.getNome());
        assertEquals("44090988888", lucas.getCpf());
        assertEquals(LocalDate.of(1995, 6, 29), lucas.getDataNascimento());
    }

    @Test
    public void criaConta() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, 2000.0);
        assertNotNull(conta.getId());
        assertEquals(TipoConta.CONTACORRENTE, conta.getTipoConta());
        assertEquals(2000.0, conta.getLimiteSaqueDiario());
        assertEquals(LocalDate.now(), conta.getDataCriacao());
        assertEquals(0.0, conta.getSaldo());
    }

    @Test
    public void criaContaLimiteInferior() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        assertThrows(BadRequestException.class, () -> lucas.criaConta(TipoConta.CONTACORRENTE, 999.00));
    }

    @Test
    public void deposita() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta = lucas.deposita(conta.getId(), 500.2);
        Optional<Transacao> optionalTransacao = transacaoRepository.findFirst1ByContaIdOrderByIdDesc(conta.getId());
        assertTrue(optionalTransacao.isPresent());
        Transacao transacao = optionalTransacao.get();
        assertEquals(LocalDate.now(), transacao.getDataTransacao().toLocalDate());
        assertEquals(TipoTransacao.DEPOSITO, transacao.getTipoTransacao());
        assertEquals(500.2, transacao.getValor());
        assertEquals(conta.getId(), transacao.getConta().getId());
        assertEquals(500.2, conta.getSaldo());
    }

    @Test
    public void depositaContaBloqueada() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta.bloqueiaConta();
        assertThrows(ForbiddenException.class, () -> lucas.deposita(conta.getId(), 500.2));
    }

    @Test
    public void depositaEmContaTerceiro() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        final Pessoa joana = pessoaRepository.save(new Pessoa("Joana aona ana", "12345678912",
                LocalDate.now()));
        Conta contaJoana = joana.criaConta(TipoConta.CONTACORRENTE, null);
        contaJoana = lucas.deposita(contaJoana.getId(), 6000.0);
        Optional<Transacao> optionalTransacao = transacaoRepository
                .findFirst1ByContaIdOrderByIdDesc(contaJoana.getId());
        assertTrue(optionalTransacao.isPresent());
        Transacao transacao = optionalTransacao.get();
        assertEquals(6000.0, contaJoana.getSaldo());
        assertEquals(LocalDate.now(), contaJoana.getDataCriacao());
        assertEquals(TipoConta.CONTACORRENTE, contaJoana.getTipoConta());
        assertEquals(Conta.LIMITE_SAQUE_DIARIO_BASE, contaJoana.getLimiteSaqueDiario());
        assertEquals(joana.getId(), contaJoana.getPessoa().getId());
        assertEquals(LocalDate.now(), transacao.getDataTransacao().toLocalDate());
        assertEquals(TipoTransacao.DEPOSITO, transacao.getTipoTransacao());
        assertEquals(6000.0, transacao.getValor());
        assertEquals(contaJoana.getId(), transacao.getConta().getId());
        assertEquals(lucas.getId(), transacao.getPessoa().getId());
        assertEquals(lucas.getNome(), transacao.getPessoa().getNome());
        assertEquals(lucas.getCpf(), transacao.getPessoa().getCpf());
        assertEquals(lucas.getDataNascimento(), transacao.getPessoa().getDataNascimento());
    }

    @Test
    public void depositaContaNaoEncontrada() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        assertThrows(NotFoundException.class, () -> lucas.deposita(987456321L, 500.20));
    }

    @Test
    public void saca() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta = lucas.deposita(conta.getId(), 1000.0);
        conta = lucas.saca(conta, 20.0);
        Optional<Transacao> optionalTransacao = transacaoRepository.findFirst1ByContaIdOrderByIdDesc(conta.getId());
        assertTrue(optionalTransacao.isPresent());
        Transacao transacao = optionalTransacao.get();
        assertEquals(LocalDate.now(), transacao.getDataTransacao().toLocalDate());
        assertEquals(TipoTransacao.SAQUE, transacao.getTipoTransacao());
        assertEquals(20.0, transacao.getValor());
        assertEquals(conta.getId(), transacao.getConta().getId());
        assertEquals(980.0, conta.getSaldo());
    }

    @Test
    public void limiteSaqueDiario() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta = lucas.deposita(conta.getId(), 3000.0);
        Conta finalConta = conta;
        assertThrows(BadRequestException.class, () -> lucas.saca(finalConta, 1001.0));
    }

    @Test
    public void sacaContaBloqueada() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        conta = lucas.deposita(conta.getId(), 1000.0);
        conta.bloqueiaConta();
        Conta finalConta = conta;
        assertThrows(ForbiddenException.class, () -> lucas.saca(finalConta, 20.0));
    }

    @Test
    public void sacaDeOutraConta() {
        Pessoa bandido = contaService.findPessoa("44090988888");
        final Pessoa joana = pessoaRepository.save(new Pessoa("Joana aona ana", "12345678912",
                LocalDate.now()));
        assertThrows(BadRequestException.class, () -> {
            Conta contaJoana = joana.criaConta(TipoConta.CONTACORRENTE, null);
            joana.deposita(contaJoana.getId(), 6000.0);
            bandido.saca(contaJoana, 4000.0);
        });
    }

    @Test
    public void sacaValorInsuficiente() {
        Pessoa lucas = contaService.findPessoa("44090988888");
        Conta conta = lucas.criaConta(TipoConta.CONTACORRENTE, null);
        assertThrows(BadRequestException.class, () -> lucas.saca(conta, 20.0));
    }
}
