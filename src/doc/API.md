# Documentação de endpoints da aplicação

| Método | URI                              | Documentação                                                                                        |
| ------ | -------------------------------- | --------------------------------------------------------------------------------------------------- |
|        |                                  | Operações de conta                                                                                  |
| PUT    | /conta/{cpf}                     | Cria uma conta para uma pessoa.                                                                     |
| POST   | /conta/{cpf}/deposita            | Deposita um valor em uma conta de uma pessoa.                                                       |
| GET    | /conta/{numeroConta}/saldo       | Consulta o saldo de uma conta.                                                                      |
| POST   | /{numeroConta}/saca              | Realiza um saque em uma conta.                                                                      |
| POST   | /{numeroConta}/bloqueia          | Bloqueia conta.                                                                                     |
| POST   | /{numeroConta}/extrato           | Emite um extrato por período de uma conta. deve-se enviar respectivamente as datas de início e fim. |