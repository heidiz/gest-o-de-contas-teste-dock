package com.testedock.gc.domain.service;

import com.testedock.gc.domain.Conta;
import com.testedock.gc.domain.Pessoa;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.PessoaRepository;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;
import com.testedock.gc.infrastructure.exceptions.NotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * Serviço de conta
 */
@Service
public class ContaService {
    private final ContaRepository contaRepository;
    private final PessoaRepository pessoaRepository;

    /**
     * Criação de serviço de gerência de dados de conta
     *
     * @param contaRepository Repositório para acesso dos dados de objeto {@link Conta}
     * @param pessoaRepository Repositório para acesso dos dados de objeto {@link Pessoa}
     */
    public ContaService(ContaRepository contaRepository, PessoaRepository pessoaRepository) {
        this.contaRepository = contaRepository;
        this.pessoaRepository = pessoaRepository;
    }

    /**
     * Busca uma pessoa través do cpf
     *
     * @param cpf Cpf de um objeto {@link Pessoa}
     *
     * @return Objeto {@link Pessoa}
     */
    public Pessoa findPessoa(String cpf) {
        if (cpf == null ) {
            throw new BadRequestException("CPF não pode ser nulo.");
        }
        if (cpf.length() != 11) {
            throw new BadRequestException("CPF inválido.");
        }

        Optional<Pessoa> optionalPessoa = pessoaRepository.findByCpf(cpf);
        if (!optionalPessoa.isPresent()) {
            throw new NotFoundException(String.format("Não existe um cliente para o cpf %s.", cpf));
        }

        return optionalPessoa.get();
    }

    /**
     * Busca uma conta através da representação de identidade
     *
     * @param id Id da conta que se fará a busca
     *
     * @return {@link Conta}
     */
    public Conta findConta(Long id) {
        Optional<Conta> optionalConta = contaRepository.findById(id);
        if (!optionalConta.isPresent()) {
            throw new NotFoundException(String.format("A conta %d não existe.", id));
        }

        return optionalConta.get();
    }
}
