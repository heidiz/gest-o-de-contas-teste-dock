package com.testedock.gc.domain;

import com.testedock.gc.domain.base.EntityBase;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.TransacaoRepository;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;
import com.testedock.gc.infrastructure.exceptions.ForbiddenException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDate;
import java.util.List;

/**
 * Conta que pode ser gerenciada por uma pessoa cliente de um banco
 * 
 */
@Entity(name = "Contas")
@Configurable
public class Conta extends EntityBase<Conta> {
    @JoinColumn(nullable = false)
    @ManyToOne
    private Pessoa pessoa;
    @Column(nullable = false)
    private Double saldo;
    @Column(nullable = false)
    private Double limiteSaqueDiario;
    @Column(nullable = false)
    private boolean flagAtivo;
    @Column(nullable = false)
    private TipoConta tipoConta;
    @Column(nullable = false)
    private LocalDate dataCriacao;

    public final static transient Double LIMITE_SAQUE_DIARIO_BASE = 1000.0;

    @Autowired
    private transient ContaRepository contaRepository;

    @Autowired
    private transient TransacaoRepository transacaoRepository;

    public Conta() {
    }

    /**
     * Criação de um objeto {@link Conta}
     * 
     * @param pessoa Objeto {@link Pessoa} para a qual se está criando a Conta
     * @param tipoConta {@link TipoConta} que será criada
     * @param limiteSaqueDiario Limite do quanto se pode sacar por dia
     */
    public Conta(final Pessoa pessoa, final TipoConta tipoConta, final Double limiteSaqueDiario) {
        this.pessoa = pessoa;
        this.tipoConta = tipoConta;
        this.saldo = 0.0;
        this.limiteSaqueDiario = LIMITE_SAQUE_DIARIO_BASE;
        if (limiteSaqueDiario != null) {
            if (limiteSaqueDiario <= LIMITE_SAQUE_DIARIO_BASE) {
                throw new BadRequestException(String.format("Limite de saque diário não pode ser inferior à %f",
                        LIMITE_SAQUE_DIARIO_BASE));
            }
            this.limiteSaqueDiario = limiteSaqueDiario;
        }

        this.flagAtivo = true;
        this.dataCriacao = LocalDate.now();
    }

    /**
     * Altera o saldo de deste objeto {@link Conta}
     * 
     * @param transacao objeto {@link Transacao} para efetuar o cálculo de geração de saldo
     */
    public void setSaldo(Transacao transacao) {
        if (!this.isFlagAtivo()) {
            throw new ForbiddenException("Conta bloqueada.");
        }
        if (transacao.getTipoTransacao() == TipoTransacao.DEPOSITO) {
            this.saldo = this.saldo + transacao.getValor();
        } else {
            this.saldo = this.saldo - transacao.getValor();
            if (this.saldo <= 0) {
                throw new BadRequestException(String.format("Saldo insuficiente na conta %d.", this.getId()));
            }
        }
    }

    /**
     * Bloqueia um objeto {@link Conta}
     */
    public void bloqueiaConta() {
        if (!this.flagAtivo) {
            throw new BadRequestException("Conta já bloqueada.");
        }
        this.flagAtivo = false;
        contaRepository.save(this);
    }

    /**
     * Retorna o extrato do objeto {@link Conta}
     * 
     * @param inicio Data de início no qual se buscará o extrato
     * @param fim Data de fim no qual se buscará o extrato
     *            
     * @return Uma lista de objetos {@link Transacao} com as transações efetuadas no período 
     */
    public List<Transacao> extrato(LocalDate inicio, LocalDate fim) {
        if (!this.isFlagAtivo()) {
            throw new ForbiddenException("Conta bloqueada.");
        }
        return transacaoRepository.findByDataTransacaoBetweenAndContaId(inicio.atStartOfDay(),
                fim.atTime(23, 59, 59), this.getId());
    }

    /**
     * Retorna a {@link Pessoa} deste objeto {@link Conta}
     * 
     * @return a {@link Pessoa} deste objeto {@link Conta}
     */
    public Pessoa getPessoa() {
        return pessoa;
    }

    /**
     * Retorna o saldo deste objeto {@link Conta}
     * 
     * @return o saldo deste objeto {@link Conta}
     */
    public Double getSaldo() {
        return saldo;
    }

    /**
     * Retorna o limite de saque diário deste objeto {@link Conta}
     *
     * @return o limite de saque diário deste objeto {@link Conta}
     */
    public Double getLimiteSaqueDiario() {
        return limiteSaqueDiario;
    }

    /**
     * Verifica se este objeto {@link Conta}
     *
     * @return Valor boloeano representando se este objeto {@link Conta} está ativo
     */
    public boolean isFlagAtivo() {
        return flagAtivo;
    }

    /**
     * Retorna o {@link TipoConta} deste objeto {@link Conta}
     *
     * @return o {@link TipoConta} deste objeto {@link Conta}
     */
    public TipoConta getTipoConta() {
        return tipoConta;
    }

    /**
     * Retorna a data de criação deste objeto {@link Conta}
     *
     * @return a data de criação deste objeto {@link Conta}
     */
    public LocalDate getDataCriacao() {
        return dataCriacao;
    }
}
