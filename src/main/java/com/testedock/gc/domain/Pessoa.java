package com.testedock.gc.domain;

import com.testedock.gc.domain.base.EntityBase;
import com.testedock.gc.infrastructure.ContaRepository;
import com.testedock.gc.infrastructure.TransacaoRepository;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;
import com.testedock.gc.infrastructure.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.transaction.Transactional;
import java.time.LocalDate;

/**
 * Pessoa cliente de um banco com a alguns métodos representando o que pode fazer.
 * (utilizei o nome pessoa por conta da especificação no requisito do teste. Vejo essa entidade como cliente)
 */
@Entity(name = "Pessoas")
@Configurable
public class Pessoa extends EntityBase<Pessoa> {
    @Column(nullable = false)
    private String nome;
    @Column(nullable = false, unique = true, length = 11)
    private String cpf;
    @Column(nullable = false)
    private LocalDate dataNascimento;

    @Autowired
    private transient ContaRepository contaRepository;

    @Autowired
    private transient TransacaoRepository transacaoRepository;


    public Pessoa() {

    }

    /**
     * Criação de uma pessoa cliente 
     * 
     * @param nome Nome da pessoa cliente
     * @param cpf Cpf da pessoa cliente
     * @param dataNascimento Data de Nascimento da pessoa cliente
     */
    public Pessoa(final String nome, final String cpf, final LocalDate dataNascimento) {
        this.nome = nome;
        this.cpf = cpf;
        this.dataNascimento = dataNascimento;
    }

    /**
     * Cria conta para esse objeto {@link Pessoa}
     * 
     * @param tipoConta {@link TipoConta} que será criado
     * @param limiteSaqueDiario Limite do quanto se pode sacar por dia
     *                          
     * @return {@link Conta} criada para essa pessoa
     */
    public Conta criaConta(final TipoConta tipoConta, final Double limiteSaqueDiario) {
        return contaRepository.save(new Conta(this, tipoConta, limiteSaqueDiario));
    }

    /**
     * Realiza um depósito em uma conta
     * 
     * @param numeroConta Número da conta na qual se fará o depósito
     * @param quantidade Valor que se depositará na conta
     * @return {@link Conta} aonde foi feito o depósito
     */
    @Transactional
    public Conta deposita(final Long numeroConta, final Double quantidade) {
        Conta conta = contaRepository.findById(numeroConta)
                .orElseThrow(() -> new NotFoundException(String.format("Conta %d não encontrada.", numeroConta)));

        Transacao transacao = new Transacao(this, conta, quantidade, TipoTransacao.DEPOSITO);
        transacaoRepository.save(transacao);
        conta.setSaldo(transacao);
        return contaRepository.save(conta);
    }

    /**
     * Saca um valor da conta deste objeto {@link Pessoa}
     * 
     * @param conta {@link Conta} de onde será feito o saque
     * @param valor Valor que será sacado da Conta
     * @return {@link Conta} aonde foi realizado o saque
     */
    @Transactional
    public Conta saca(final Conta conta, final Double valor) {
        if (!conta.getPessoa().getId().equals(this.getId())) {
            throw new BadRequestException(String.format("Conta não pertencente à %s.", this.getNome()));
        }

        if (valor.compareTo(conta.getLimiteSaqueDiario()) > 0) {
            throw new BadRequestException(String.format("Não é possível sacar um valor maior que o limite de %f.",
                    conta.getLimiteSaqueDiario()));
        }

        double sumDia = transacaoRepository
                .findByDataTransacaoBetweenAndContaIdAndTipoTransacao(LocalDate.now().atStartOfDay(),
                        LocalDate.now().atTime(23, 59, 59), conta.getId(), TipoTransacao.SAQUE)
                .parallelStream().mapToDouble(Transacao::getValor).sum();

        if (sumDia + valor > conta.getLimiteSaqueDiario()) {
            throw new BadRequestException("Limite diário atingido.");
        }

        Transacao transacao = new Transacao(null, conta, valor, TipoTransacao.SAQUE);
        conta.setSaldo(transacao);
        transacaoRepository.save(transacao);
        return contaRepository.save(conta);
    }

    /**
     * Retorna o nome deste objeto {@link Pessoa}
     * 
     * @return o nome deste objeto {@link Pessoa}
     */
    public String getNome() {
        return nome;
    }

    /**
     * Retorna o cpf deste objeto {@link Pessoa}
     * 
     * @return o cpf deste objeto {@link Pessoa}
     */
    public String getCpf() {
        return cpf;
    }

    /**
     * Retorna a data de nascimento deste objeto {@link Pessoa}
     * 
     * @return a data de nascimento deste objeto {@link Pessoa}
     */
    public LocalDate getDataNascimento() {
        return dataNascimento;
    }
}
