package com.testedock.gc.domain;

/**
 * Tipo de Conta
 *
 */
public enum TipoConta {
    /**
     * Conta corrrente
     */
    CONTACORRENTE(1),

    /**
     * Conta poupança
     */
    CONTAPOUPANCA(2),

    /**
     * Conta salário
     */
    CONTASALARIO(3),

    /**
     * Conta digital
     */
    CONTADIGITAL(4);

    private int value;

    TipoConta(int value) {
        this.value = value;
    }
}
