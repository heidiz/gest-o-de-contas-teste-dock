package com.testedock.gc.domain;

/**
 * Tipo de transação
 *
 */
public enum TipoTransacao {
    /**
     * Depósito
     */
    DEPOSITO(1),

    /**
     * Saque
     */
    SAQUE(2);

    private int value;

    TipoTransacao(int value) {
        this.value = value;
    }
}
