package com.testedock.gc.domain;

import com.testedock.gc.domain.base.EntityBase;
import com.testedock.gc.infrastructure.exceptions.BadRequestException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

/**
 * Transação de uma conta
 *
 */
@Entity(name = "Transacoes")
public class Transacao extends EntityBase<Transacao> {
    @JoinColumn(nullable = false)
    @ManyToOne
    private Conta conta;
    @Column(nullable = false)
    private Double valor;
    @Column(nullable = false)
    private LocalDateTime dataTransacao;

    @JoinColumn
    @ManyToOne
    private Pessoa pessoa;

    @Column(nullable = false)
    private TipoTransacao tipoTransacao;

    /**
     * Criação de um objeto {@link Transacao}
     *
     * @param pessoa {@link Pessoa} que efetuou uma transação de depósito
     * @param conta {@link Conta} aonde foi realizada a transação
     * @param valor Valor que será transacionado
     * @param tipoTransacao {@link TipoTransacao}
     */
    public Transacao(final Pessoa pessoa, final Conta conta, final Double valor, final TipoTransacao tipoTransacao) {
        this.pessoa = pessoa;
        this.conta = conta;
        if (valor <= 0) {
            throw new BadRequestException("Valor não pode ser igual ou inferior à zero.");
        }
        this.valor = valor;
        this.dataTransacao = LocalDateTime.now();
        this.tipoTransacao = tipoTransacao;
    }

    /**
     * Criação de um objeto {@link Transacao}
     *
     * @param pessoa {@link Pessoa} que efetuou uma transação de depósito
     * @param conta {@link Conta} aonde foi realizada a transação
     * @param valor Valor que será transacionado
     * @param tipoTransacao {@link TipoTransacao}
     * @param dataTransacao Data da transação
     */
    public Transacao(final Pessoa pessoa, final Conta conta, final Double valor, final TipoTransacao tipoTransacao,
                     LocalDateTime dataTransacao) {
        this.pessoa = pessoa;
        this.conta = conta;
        if (valor <= 0) {
            throw new BadRequestException("Valor não pode ser igual ou inferior à zero.");
        }
        this.valor = valor;
        this.dataTransacao = dataTransacao;
        this.tipoTransacao = tipoTransacao;
    }

    public Transacao() {
    }

    /**
     * Retorna o objeto {@link Conta} deste objeto {@link Transacao}
     *
     * @return o objeto {@link Conta} deste objeto {@link Transacao}
     */
    public Conta getConta() {
        return conta;
    }

    /**
     * Retorna o valor transacionado deste objeto {@link Transacao}
     *
     * @return o valor transacionado deste objeto {@link Transacao}
     */
    public Double getValor() {
        return valor;
    }

    /**
     * Retorna a data da transação deste objeto {@link Transacao}
     *
     * @return a data da transação deste objeto {@link Transacao}
     */
    public LocalDateTime getDataTransacao() {
        return dataTransacao;
    }

    /**
     * Retorna a pessoa que realizou um depósito
     *
     * @return a pessoa que realizou um depósito
     */
    public Pessoa getPessoa() {
        return pessoa;
    }

    /**
     * Retorna o {@link TipoTransacao}
     * @return o {@link TipoTransacao}
     */
    public TipoTransacao getTipoTransacao() {
        return tipoTransacao;
    }
}
