package com.testedock.gc.infrastructure.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Conflict http status 409
 */
@ResponseStatus(HttpStatus.CONFLICT)
public class ConflitctException extends RuntimeException {
    public ConflitctException() {
        super();
    }

    public ConflitctException(String message) {
        super(message);
    }

    public ConflitctException(String message, Throwable cause) {
        super(message, cause);
    }

    public ConflitctException(Throwable cause) {
        super(cause);
    }

    public ConflitctException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
