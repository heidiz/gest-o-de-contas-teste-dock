package com.testedock.gc.infrastructure;

import com.testedock.gc.domain.Conta;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Repositório para acesso dos dados de objeto {@link Conta}
 */
@Repository
public interface ContaRepository extends JpaRepository<Conta, Long> {
    List<Conta> findByPessoaCpf(String cpf);
}
