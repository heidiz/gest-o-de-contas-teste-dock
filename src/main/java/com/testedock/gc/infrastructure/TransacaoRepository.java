package com.testedock.gc.infrastructure;

import com.testedock.gc.domain.TipoTransacao;
import com.testedock.gc.domain.Transacao;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Repositório para acesso dos dados de objeto {@link Transacao}
 */
public interface TransacaoRepository extends JpaRepository<Transacao, Long> {
    /**
     * Busca uma lista de transações de uma conta em um período
     *
     * @param inicio Data de início do período
     * @param fim Data de fim do período
     * @param idConta Id da conta
     *
     * @return Uma lista de transações {@link Transacao} de uma conta de um período
     */
    List<Transacao> findByDataTransacaoBetweenAndContaId(LocalDateTime inicio, LocalDateTime fim, Long idConta);

    /**
     * Busca uma lista de transações de uma conta em um período
     *
     * @param inicio Data de início do período
     * @param fim Data de fim do período
     * @param idConta Id da conta
     * @param tipoTransacao {@link TipoTransacao} que se deseja buscar
     *
     * @return Uma lista de transações {@link Transacao} de uma conta de um período
     */
    List<Transacao> findByDataTransacaoBetweenAndContaIdAndTipoTransacao(LocalDateTime inicio, LocalDateTime fim,
                                                                         Long idConta, TipoTransacao tipoTransacao);

    /**
     * Busca a última transação de uma conta
     *
     * @param id Id da conta da transação
     *
     * @return a última {@link Transacao} de uma conta
     */
    Optional<Transacao> findFirst1ByContaIdOrderByIdDesc(Long id);
}
