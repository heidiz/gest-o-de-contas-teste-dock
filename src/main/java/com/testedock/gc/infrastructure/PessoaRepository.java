package com.testedock.gc.infrastructure;

import com.testedock.gc.domain.Pessoa;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

/**
 * Repositório para acesso dos dados de objeto {@link Pessoa}
 */
public interface PessoaRepository extends JpaRepository<Pessoa, Long> {
    /**
     * Busca um pessoa por um cpf
     *
     * @param cpf Cpf de um objeto {@link Pessoa}
     *
     * @return Um objeto {@link Pessoa}
     */
    Optional<Pessoa> findByCpf(String cpf);
}
