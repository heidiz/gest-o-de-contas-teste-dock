package com.testedock.gc.ui.controller.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * Dados de saque para requisição
 */
public class Saque {
    @ApiModelProperty(value = "Valor que será sacado", required = true)
    @NotNull(message = "Valor não pode ser nulo.")
    @DecimalMin(value = "5.00", message = "O Valor não deve ser inferior à R$ 5,00")
    private Double valor;

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
