package com.testedock.gc.ui.controller.model;

import com.testedock.gc.domain.TipoConta;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;

/**
 * Dados para criação de uma nova conta
 */
public class NovaConta {
    @ApiModelProperty(value = "Tipo da conta", required = true)
    @NotNull(message = "O tipo da conta não pode ser nulo.")
    private TipoConta tipoConta;

    @ApiModelProperty(value = "Limite de saque diário")
    @DecimalMin(value = "1000.00", message = "O limite de saque não deve ser inferior à R$ 1000,00.")
    private Double limiteSaqueDiario;

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public Double getLimiteSaqueDiario() {
        return limiteSaqueDiario;
    }

    public void setLimiteSaqueDiario(Double limiteSaqueDiario) {
        this.limiteSaqueDiario = limiteSaqueDiario;
    }
}
