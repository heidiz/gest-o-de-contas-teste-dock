package com.testedock.gc.ui.controller.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

/**
 * Dados para requisição de busca de extrato por período
 */
public class Periodo {
    @ApiModelProperty(value = "Data de início do período", required = true)
    @NotNull(message = "Data início não pode ser nula.")
    LocalDate inicio;

    @ApiModelProperty(value = "Data fim do período", required = true)
    @NotNull(message = "Data fim não pode ser nula.")
    LocalDate fim;

    public LocalDate getInicio() {
        return inicio;
    }

    public void setInicio(LocalDate inicio) {
        this.inicio = inicio;
    }

    public LocalDate getFim() {
        return fim;
    }

    public void setFim(LocalDate fim) {
        this.fim = fim;
    }
}
