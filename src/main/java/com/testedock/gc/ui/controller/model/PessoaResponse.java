package com.testedock.gc.ui.controller.model;

import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDate;

/**
 * Dados de uma pessoa
 */
public class PessoaResponse {
    @ApiModelProperty(value = "Nome da pessoa")
    private String nome;
    @ApiModelProperty(value = "Cpf da pessoa")
    private String cpf;
    @ApiModelProperty(value = "Data de nascimento")
    private LocalDate dataNascimento;

    public PessoaResponse(com.testedock.gc.domain.Pessoa pessoa) {
        this.nome = pessoa.getNome();
        this.cpf = pessoa.getCpf();
        this.dataNascimento = pessoa.getDataNascimento();
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }
}
