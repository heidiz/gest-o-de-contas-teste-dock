package com.testedock.gc.ui.controller.model;

import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotNull;

/**
 * Dados de depósito para requisição
 */
public class Deposito {
    @NotNull(message = "Número da conta não pode ser nulo.")
    @ApiModelProperty(value = "Id da Conta", required = true)
    private Long numeroConta;
    @NotNull(message = "Valor não pode ser nulo.")
    @ApiModelProperty(value = "Valor que será utilizado para depósito", required = true)
    private Double valor;

    public Long getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Long numeroConta) {
        this.numeroConta = numeroConta;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }
}
