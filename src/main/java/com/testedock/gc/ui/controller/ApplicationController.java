package com.testedock.gc.ui.controller;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;

@RestController
public class ApplicationController {
    private final String className = getClass().getSimpleName();

    @GetMapping(path = "/")
    public ResponseEntity<String> home(final HttpMethod method,
                                       final WebRequest request) {
        _print(method, request);
        final String htmlContent = "<!DOCTYPE html><html><body>"
                + "<p style='font-size: large;'>Clique aqui para <a href='swagger-ui.html'>documentação da API</a></p>"
                + "</body></html>";
        final ResponseEntity<String> responseEntity = new ResponseEntity<>(htmlContent, HttpStatus.OK);
        return responseEntity;
    }

    private void _print(final HttpMethod method, final WebRequest request) {
        System.out.printf("%s %s %s\n", className, method, request);
    }
}
