package com.testedock.gc.ui.controller.model;

import com.testedock.gc.domain.Conta;
import com.testedock.gc.domain.Transacao;
import io.swagger.annotations.ApiModelProperty;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Dados de extrato de uma conta
 */
public class Extrato {
    @ApiModelProperty(value = "Conta do extrato")
    private ContaResponse conta;
    @ApiModelProperty(value = "Transações do extrato")
    private List<TransacaoResponse> transacoes;

    public Extrato(Conta conta, List<Transacao> transacoes) {
        this.conta = new ContaResponse(conta);
        this.transacoes = transacoes.parallelStream()
                .map(TransacaoResponse::new).collect(Collectors.toList());
    }

    public ContaResponse getConta() {
        return conta;
    }

    public void setConta(ContaResponse conta) {
        this.conta = conta;
    }

    public List<TransacaoResponse> getTransacoes() {
        return transacoes;
    }

    public void setTransacoes(List<TransacaoResponse> transacoes) {
        this.transacoes = transacoes;
    }
}
