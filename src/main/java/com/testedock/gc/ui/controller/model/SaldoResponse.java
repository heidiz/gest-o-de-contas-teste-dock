package com.testedock.gc.ui.controller.model;

import com.testedock.gc.domain.Conta;
import io.swagger.annotations.ApiModelProperty;

/**
 * Saldo de uma conta
 */
public class SaldoResponse {
    @ApiModelProperty(value = "Id da conta")
    private Long numeroConta;
    @ApiModelProperty(value = "Pessoa dona da conta")
    private PessoaResponse pessoa;
    @ApiModelProperty(value = "Saldo da conta")
    private Double saldo;

    public SaldoResponse(Conta conta) {
        this.numeroConta = conta.getId();
        this.pessoa = new PessoaResponse(conta.getPessoa());
        this.saldo = conta.getSaldo();
    }

    public Long getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(Long numeroConta) {
        this.numeroConta = numeroConta;
    }

    public PessoaResponse getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaResponse pessoa) {
        this.pessoa = pessoa;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }
}
