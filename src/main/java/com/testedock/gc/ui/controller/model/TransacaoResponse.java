package com.testedock.gc.ui.controller.model;

import com.testedock.gc.domain.TipoTransacao;
import com.testedock.gc.domain.Transacao;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.time.LocalDateTime;

/**
 * Dados de transação para resposta de requisição
 */
public class TransacaoResponse {
    @ApiModelProperty(value = "Tipo da transação")
    private String tipoTransacao;
    @ApiModelProperty(value = "Valor da transação")
    private Double valor;
    @ApiModelProperty(value = "Data da transação")
    private LocalDateTime dataTransacao;
    @ApiModelProperty(value = "Pessoa que fez um depósito")
    private PessoaResponse pessoa;

    public TransacaoResponse(Transacao transacao) {
        this.tipoTransacao = transacao.getTipoTransacao().equals(TipoTransacao.DEPOSITO) ? "Depósito" : "Saque";
        this.valor = transacao.getValor();
        this.dataTransacao = transacao.getDataTransacao();
        if (transacao.getPessoa() != null) {
            this.pessoa = new PessoaResponse(transacao.getPessoa());
        }
    }

    public String getTipoTransacao() {
        return tipoTransacao;
    }

    public void setTipoTransacao(String tipoTransacao) {
        this.tipoTransacao = tipoTransacao;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public LocalDateTime getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(LocalDateTime dataTransacao) {
        this.dataTransacao = dataTransacao;
    }

    public PessoaResponse getPessoa() {
        return pessoa;
    }

    public void setPessoa(PessoaResponse pessoa) {
        this.pessoa = pessoa;
    }
}
