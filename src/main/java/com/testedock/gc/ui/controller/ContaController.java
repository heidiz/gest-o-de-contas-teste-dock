package com.testedock.gc.ui.controller;

import com.testedock.gc.domain.Conta;
import com.testedock.gc.domain.Pessoa;
import com.testedock.gc.domain.service.ContaService;
import com.testedock.gc.ui.controller.model.*;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/conta")
public class ContaController {
    private final ContaService contaService;

    public ContaController(ContaService contaService) {
        this.contaService = contaService;
    }

    @ApiOperation(value = "Cria uma conta para uma pessoa.")
    @PutMapping("/{cpf}")
    public ResponseEntity<ContaResponse> criaConta(@PathVariable String cpf,
                                                   @RequestBody @Validated NovaConta novaConta) {
        Pessoa pessoa = contaService.findPessoa(cpf);
        Conta conta = pessoa.criaConta(novaConta.getTipoConta(), novaConta.getLimiteSaqueDiario());
        return new ResponseEntity<>(new ContaResponse(conta), HttpStatus.CREATED);
    }

    @ApiOperation(value = "Deposita um valor em uma conta de uma pessoa.")
    @PostMapping("/{cpf}/deposita")
    public ResponseEntity<ContaResponse> deposita(@PathVariable String cpf, @RequestBody @Validated Deposito deposito) {
        Pessoa pessoa = contaService.findPessoa(cpf);
        Conta conta = pessoa.deposita(deposito.getNumeroConta(), deposito.getValor());
        return new ResponseEntity<>(new ContaResponse(conta), HttpStatus.OK);
    }

    @ApiOperation(value = "Consulta o saldo de uma conta.")
    @GetMapping("/{numeroConta}/saldo")
    public ResponseEntity<SaldoResponse> consultaSaldo(@PathVariable Long numeroConta) {
        return new ResponseEntity<>(new SaldoResponse(contaService.findConta(numeroConta)), HttpStatus.OK);
    }

    @ApiOperation(value = "Realiza um saque em uma conta.")
    @PostMapping("/{numeroConta}/saca")
    public ResponseEntity<ContaResponse> saca(@PathVariable Long numeroConta,
                                              @RequestBody @Validated Saque saque) {
        Conta conta = contaService.findConta(numeroConta);
        conta.getPessoa().saca(conta, saque.getValor());
        return new ResponseEntity<>(new ContaResponse(conta), HttpStatus.OK);
    }

    @ApiOperation(value = "Bloqueia conta.")
    @PostMapping("/{numeroConta}/bloqueia")
    @ResponseStatus(HttpStatus.OK)
    public void bloqueaConta(@PathVariable Long numeroConta) {
        Conta conta = contaService.findConta(numeroConta);
        conta.bloqueiaConta();
    }

    @ApiOperation(value = "Emite um extrato por período de uma conta. " +
            "deve-se enviar respectivamente as datas de início e fim. ")
    @PostMapping("/{numeroConta}/extrato")
    public ResponseEntity<Extrato> pegaExtrato(@PathVariable Long numeroConta,
                                               @RequestBody @Validated Periodo periodo) {
        Conta conta = contaService.findConta(numeroConta);
        return new ResponseEntity<>(new Extrato(conta, conta.extrato(periodo.getInicio(), periodo.getFim())),
                HttpStatus.OK);
    }
}
