package com.testedock.gc;

import com.testedock.gc.domain.Pessoa;
import com.testedock.gc.infrastructure.PessoaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.aspectj.EnableSpringConfigured;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootApplication
@EnableSpringConfigured
@EnableAspectJAutoProxy
public class GcApplication {

    public static void main(String[] args) {
        SpringApplication.run(GcApplication.class, args);
    }

    /**
     * Será criada uma pessoa no banco no caso de se estar usando banco em memória
     *
     * @param pessoaRepository Repositório para gerenciar dados de um objeto {@link Pessoa}
     *
     * @return {@link CommandLineRunner}
     */
    @Bean
    @Autowired
    public CommandLineRunner startCommandLineRunner(final PessoaRepository pessoaRepository) {
        Optional<Pessoa> optionalLucas = pessoaRepository.findByCpf("44090988888");
        if (!optionalLucas.isPresent()) {
            pessoaRepository.save(new Pessoa("Lucas Souza Pessoa", "44090988888",
                    LocalDate.of(1995, 6, 29)));
        }


        return args -> System.out.println("started");
    }

}
