Projeto de Gerenciamento de Conta
=================================

Este projeto foi desenvolvido para avaliação técnica no teste de nivelamento da Dock.

Neste projeto foram aplicados conceitos de Domain Driven Design
    
Este projeto utiliza:
JDK 8
Spring Boot
Spring Data
JPA
Aspecj
Junit 5

## Documentação API
As apis deste projeto foram documentadas via swagger. Para acessá-lo, inicie a aplicação e [clique aqui](http://127.0.0.1:8080/swagger-ui.html) (para autenticação, leia o conteúdo de `Acessando as apis`)

## Como executar o projeto
- Banco:
    - Existem duas formas para se acessar banco.
        - A primeira e mais fácil é ir no application.properties `do aplicativo` e comentar o bloco `SQL SERVER` e descomentar o bloco `banco em memória`.
        - A segunda, consiste na necessidade de executar os scripts na pasta [sql](src/sql) em um banco sql server sem precisar mexer no application.properties 
        
- Execução via maven:
    - No diretório do projeto execute o comando: mvn spring-boot:run

## Acessando as apis
Pode-se realizar requisições normalmente através do swagger (acesso especificado acima), ou ainda realizar requisições através de outras aplicações.
Em ambos os casos, existe a necessidade de se fazer uma autenticação básica.

Através do swagger, aparecerá um popup que pedirá credenciais. Sendo elas `UserName= admin, Password= password`.

Para acesso à api de outra forma, a autenticação depende da inclusão do header `Authorization: Basic xxx` nas requisições. Deve-se passar as mesmas credencias para acesso ao swagger em base 64.
Uma requisição para exemplicar ficaria assim:

`GET http://localhost:8080/conta/1/saldo
accept: */*
Authorization: Basic YWRtaW46cGFzc3dvcmQ=`
 
    
## Referências
- [DDD – Introdução a Domain Driven Design  ](http://www.agileandart.com/2010/07/16/ddd-introducao-a-domain-driven-design/)
- [Domain-Driven Design: Atacando as Complexidades no Coração do Software](https://www.amazon.com.br/Domain-Driven-Design-Eric-Evans/dp/8550800651/ref=sr_1_1?__mk_pt_BR=%C3%85M%C3%85%C5%BD%C3%95%C3%91&crid=3F2DNA4MW4SSI&keywords=ddd+eric+evans&qid=1581803365&sprefix=ddd%2Caps%2C284&sr=8-1)

![alt text](src/doc/camadas.png)